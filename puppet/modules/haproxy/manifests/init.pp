class haproxy (
  $log_maxage = 8,
  $log_rotate = 7,
  $log_level = "err",
  $timeout_connect = 15000,
  $timeout_client = 1200000,
  $timeout_server = 1200000,
  $maxconn = 3000,
  $main_site = "example.erecruit.com",
  $mobile_site = "examplemobile.erecruit.com",
  $main_site_ssl_key = "/etc/ssl/private/server.key",
  $mobile_site_ssl_key = "/etc/ssl/private/server.key",
  $main_site_httpchk = "/",
  $mobile_site_httpchk = "/",
  $httpchk_interval = 5000,
  $stats_auth_user = "haproxy",
  $stats_auth_password = "=aKwe*99h",
  $stats_auth_realm = "haproxy",
  $servers) {

  package { "haproxy" :
    ensure => "1.5.18-1ppa1~trusty", # force 1.5 since the config file format for 1.6 is different
  }

  service { "haproxy" :
     ensure => running
  }

  file { "/etc/haproxy" :
    ensure => directory,
    require => Package["haproxy"],
    owner => "root",
    group => "root",
    mode => 0744
  }

  file { "/etc/haproxy/errors" :
    ensure => directory,
    owner => "root",
    group => "root",
    mode => 0744,
    require => File["/etc/haproxy"]
  }

  file { "/etc/haproxy/haproxy_active.cfg" :
    ensure => file,
    mode   => 0644,
    owner => "root",
    group => "root",
    content => template("haproxy/etc_haproxy_haproxy_active.cfg.erb"),
    require => Package["haproxy"],
    notify => Service["haproxy"]
  }

  file { "/etc/haproxy/haproxy_maint.cfg" :
    ensure => file,
    mode   => 0644,
    owner => "root",
    group => "root",
    content => template("haproxy/etc_haproxy_haproxy_maint.cfg.erb"),
    require => Package["haproxy"],
    notify => Service["haproxy"]
  }

  file { "/etc/haproxy/haproxy.cfg" :
    ensure => link,
    target => "/etc/haproxy/haproxy_active.cfg",
    require => File["/etc/haproxy/haproxy_active.cfg"],
  }	    

  file { "/etc/logrotate.d/haproxy.dpkg-dist" :
    ensure => absent
  }

  file { "/etc/logrotate.d/haproxy" :
    ensure => file,
    mode => 0644,
    owner => "root",
    group => "root",
    content => template("haproxy/etc_logrotate.d_haproxy.erb")
  }

  file { "/etc/ssl/private/server.key" :
    ensure => file,
    mode => 0600,
    owner => "root",
    group => "root",
    source => "puppet:///modules/haproxy/server.key"
  }

  file { "/etc/haproxy/errors/503.http.maint" :
    ensure => file,
    mode => 0600,
    owner => "root",
    group => "root",
    source => "puppet:///modules/haproxy/503.http.maint",
    require => File["/etc/haproxy/errors"]
  }

}
