# Quick Start

Puppet scripts to set up an HAProxy load balancer on an Azure VM. The
presumption is that you have root access to the machine.

1. Copy this project to the target machine, for example, by cloning
   it:

```
sudo apt-get install git
git clone https://swerecruit@bitbucket.org/swerecruit/systems-architecture-haproxy.git
```

2. Run the install script:

```
cd /path/to/where/you/put/systems-architecture-haproxy
sudo bin/install.sh
```

This will install Puppet itself, and stage the modules to /etc/puppet.

3. Edit /etc/puppet/site.pp

You'll need to specify the addresses of the backend servers and
path to the SSL certs (if any). Refer to the comments in the file
for guidance on the format of these options. If you don't specify
an SSL certificate, it won't create a proxy on port 443.

4. Run puppet

```
sudo puppet apply /etc/puppet/site.pp
```

5. Verify:

You should be able to point a browser to the stats page (port 1936)
on the machine and verify that all of the backends you've configured
are active.

# Converting SSL Keys/Certs using OpenSSH

By default, this tool will attempt configure HAProxy to terminate
SSL. On Ubuntu, SSL certs are stored in the directory `/etc/ssl/certs`
and private keys are stored in `/etc/ssl/private`.

In addition, the expected format for these files is unprotected PEM.
Often client certs are delivered in password protected PFX/P12 format.
You can use openssl to convert these files as follows.

Assuming your P12/PFX file is named `bundle.p12`:

1. Extract the intermediate/CA certs from the p12 bundle and
   copy them to the system certs folder:

```openssl pkcs12 -in bundle.p12 -out server.ca-bundle -cacerts -nokeys```

If this doesn't produce any output (or produces a zero length file), this
means that the CA intermediate certs aren't in the P12/PFX bundle and you'll
need to obtain them separately.

2. Extract the private key and certificate. Note that these need to
   be bundled together in the same file.

a. Extract the key file:

```
openssl pkcs12 -in bundle.p12 -out server.key -nodes -nocerts
```

Make sure that the key file is not password protected. You can remove a password
from an RSA key file with the following command:

```
openssl rsa -in server.key -out server-nopass.key
```

Substitute `dsa` for `rsa` in the above example if you are dealing with a DSA key.


b. Extract the certificate:

```
openssl pkcs12 -in bundle.p12 -clcerts -nokeys -out server.crt
```

c. Combine them into a single file:

```
cat server-nopass.key server.crt > combined-server.key
```

The order in which the keys appear in the file is important. If you run into problems always
check that the private key comes first, the certificate second, and any intermediate/CA
certs last.


3. Move the files into the locations where HAProxy expects to find them:

```
sudo cp combined-server.key /etc/ssl/private/server.key
sudo cp ca-bundle /etc/ssl/certs/server.ca-bundle
```